extends Node2D


func _process(delta):
    var velocity = get_parent().linear_velocity
    var force = get_parent().applied_force
    var angular_velocity = get_parent().angular_velocity
    
    $velocity.rect_size.x = velocity.length()
    $velocity.rect_rotation = (velocity.angle() - get_parent().rotation)/PI*180
    
    $force.rect_size.x = force.length()
    $force.rect_rotation = (force.angle() - get_parent().rotation)/PI*180
    
    $angular_velocity.rect_size.x = abs(angular_velocity) * 100
    $angular_velocity.rect_rotation = 180 if angular_velocity > 0 else 0

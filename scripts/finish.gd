extends StaticBody2D

signal finished

onready var finish_timer = $timer 
onready var countdown = $countdown 
onready var finished = $finished

var stopped = false


func _on_area_entered(area):
    if not stopped:
        finish_timer.start()
        countdown.frame = 1
        countdown.playing = true


func _on_area_exited(area):
    if not stopped:    
        finish_timer.stop()
        countdown.frame = 0
        countdown.playing = false


func _on_timer_timeout():
    print('win')
    stopped = true
    finished.visible = true
    emit_signal('finished')

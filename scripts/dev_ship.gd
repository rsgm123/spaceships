extends RigidBody2D


onready var arrow = $arrow/arrow
onready var engines = $engines.get_children()
onready var finish_pad = get_node('../../finish_pad')

var forces = []


func _physics_process(delta):
    if position:
        arrow.position = position + Vector2(0, -200)
        arrow.rotation = Vector2.UP.angle_to(finish_pad.position - position)


func _integrate_forces(state):
    applied_force = Vector2(0, 0)
    applied_torque = 0
    forces = []
    
    if Input.is_key_pressed(KEY_A):
        var engine = engines[0]
        var pos = engine.global_position - global_position
        var force = Vector2(0, -10)
        forces.append([pos, force])
        state.add_force(pos, force.rotated(rotation))
        
    if Input.is_key_pressed(KEY_S):
        var engine = engines[1]
        var pos = engine.global_position - global_position
        var force = Vector2(0, -10)
        forces.append([pos, force])
        add_force(pos, force.rotated(rotation))
        
    if Input.is_key_pressed(KEY_D):
        var engine = engines[2]
        var pos = engine.global_position - global_position
        var force = Vector2(0, -10)
        forces.append([pos, force])
        add_force(pos, force.rotated(rotation))
        
    if Input.is_key_pressed(KEY_F):
        var engine = engines[3]
        var pos = engine.global_position - global_position
        var force = Vector2(0, -10)
        forces.append([pos, force])
        add_force(pos, force.rotated(rotation))

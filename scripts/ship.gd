extends RigidBody2D


const ENGINE = preload('res://scenes/components/engine.tscn')

const ENGINE_Y = 26
const ENGINE_X_WIDTH = 120

onready var arrow = $arrow/arrow
onready var engines = $engines
onready var finish_pad = get_node('../../finish_pad')


func _ready():
    Network.send('setController', {
        'vertical': false,
        'modules': [
            {'module': 'TextButton', 'options': {'buttons': ['go']}},
    ]})
    
    var players = Network.players.values()
    var spacing = ENGINE_X_WIDTH / (len(players) - 1)
    players.shuffle()
    
    for e in Utils.enumerate(players):
        var engine = ENGINE.instance()
        engine.player = e.object
        
        var pos_x = spacing * e.index - ENGINE_X_WIDTH / 2
        engine.position = Vector2(pos_x, ENGINE_Y)
        engines.add_child(engine)


func _physics_process(delta):
    if position:
        arrow.position = position + Vector2(0, -200)
        arrow.rotation = Vector2.UP.angle_to(finish_pad.position - position)


func _integrate_forces(state):
    applied_force = Vector2(0, 0)
    applied_torque = 0
    
    var force = Vector2(0, -40 / len(engines))
    for engine in engines.get_children(): 
        if engine.firing:
            var pos = engine.global_position - global_position
            state.add_force(pos, force.rotated(rotation))

extends Node2D


onready var particles = $Particles2D

var player
var color
var firing = false


func _ready():
    if player:
        player.connect('input', self, '_input_handler')
        $engine.modulate = Color(Utils.PLAYER_COLORS[player.data.color])        
    
    if color:
        $engine.modulate = color


func _input_handler(p, content):
    var action = content['data']['action']
    var key = content['data']['key']
    
    if key == 'go':
        if action == 'keyDown':
            firing = true
            particles.set_emitting(true)
        if action == 'keyUp':
            firing = false
            particles.set_emitting(false)
